# -*- coding: utf-8 -*-
#/usr/bin/ruby

require 'eventmachine'
require 'google_reader_api'
require 'openssl'

module GoogleReaderApi
  class Api
    def token
      @token ||= get_link('api/0/token', {})#{:ck => Time.now, :client => 'GoogleReaderApi 0.4.0'})
    end
  end

  class Entry
    def edit_tag(tag_identifier)
      @api.post_link "api/0/edit-tag" , {:a => tag_identifier,
                                        :s => entry.parent.id.content.to_s.scan(/feed\/.*/),
                                        :i => entry.id.content.to_s,
                                        :T => @api.token}
    end
  end
end

# 使ってない
class FeedPrRemover
  def initialize(feed)
    @feed = feed
  end

  def get_pr_entry
    begin
      return {} if @feed.unread_count == 0
      #puts "unread: #{@feed.unread_count}"
      @pr = @feed.all_unread_items.select{|entry|
          entry.entry.title.content.to_s.include?('PR:')
      }
    rescue => ex
      puts "error occurered. \n#{ex}\r"
      @pr
    end
  end

  def toggle_read(pr)
    return unless pr || pr.count == 0
    puts "pr: #{pr.count}"
    pr.each do |entry|
      puts entry.entry.title.content
      begin
        entry.toggle_read
        puts "toggle read!"
      rescue => exception
        puts "toggle_read error: \n#{exception}\n"
        exit 0
      end
    end
  end
end

@user = GoogleReaderApi::User.new({ :email => ENV['GMAIL'], :password => ENV['GMAIL_PASSWORD']})
feeds = @user.feeds
EM.run do
  count = feeds.count
  get_pr = proc do
    pr = {}
    if feed = feeds.pop
      begin
        next pr if feed.unread_count == 0
        #puts "unread: #{@feed.unread_count}"
        pr = feed.all_unread_items.select{|entry|
            entry.entry.title.content.to_s.include?('PR:')
        }
      rescue => ex
        puts "error occurered. \n#{ex}\r"
      end
    end
    pr
  end

  toggle_read = proc do |pr|
    count -= 1
    if (count == 0)
      EM.stop
    end

    next unless pr || pr.count == 0
    puts "pr: #{pr.count}"
    pr.each do |entry|
      puts entry.entry.title.content
      begin
        entry.toggle_read
        puts "toggle read!"
      rescue => exception
        puts "toggle_read error: \n#{exception}\n"
        exit 0
      end
    end
  end

  feeds.each do |feed|
    EM.defer(get_pr, toggle_read)
  end
end

